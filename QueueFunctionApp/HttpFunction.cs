using System.Linq;
using System.Threading.Tasks;
using AzureFunctions.Autofac;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage.Queue;

namespace QueueFunctionApp
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class HttpFunction
    {
        [FunctionName("HttpFunction")]
        public static async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "get", Route = "queue")] HttpRequest req,
                                                    [Inject] IStorageDataProvider storage
        )
        {
            string name = req.Query["name"];

            var userAgent = req.Headers.First(x => x.Key == "User-Agent").Value.First();
            if (userAgent.Contains("Rest"))
            {
                await storage.AddMessageToQueue("myqueue-items", new CloudQueueMessage(name));
            }
            else
            {
                await storage.AddDataToBlob("blob1", "blob1", name);
            }

            return name != null
                ? (ActionResult) new OkObjectResult($"Hello, {name}")
                : new BadRequestObjectResult("Please pass a name on the query string or in the request body");
        }
    }
}