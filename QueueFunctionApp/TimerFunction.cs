using System.Threading.Tasks;
using AzureFunctions.Autofac;
using Microsoft.Azure.WebJobs;

namespace QueueFunctionApp
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class TimerFunction
    {
        [FunctionName("TimerFunction")]
        public static async Task Run([TimerTrigger("0 */2 * * * *")] TimerInfo myTimer,
                                     [Inject] IStorageDataProvider storage)
        {

        }
    }
}