﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Queue;

namespace QueueFunctionApp
{
    public interface IStorageDataProvider
    {
        Task<IEnumerable<CloudQueueMessage>> GetAllMessagesFromQueue(string queueName, int messageCount);
        Task AddMessagesToQueue(string queueName, IEnumerable<CloudQueueMessage> messages);
        Task AddMessageToQueue(string queueName, CloudQueueMessage message);
        Task AddDataToBlob(string blobContainer, string blobName, string content);
        Task<string> GetAllContentFromBlob(string blobContainer, string blobName);
        Task<List<string>> GetAllContentFromBlob(string blobContainer);

        Task ClearQueue(string queueName);
    }
}