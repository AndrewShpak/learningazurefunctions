using System.Threading.Tasks;
using AzureFunctions.Autofac;
using Microsoft.Azure.WebJobs;

namespace QueueFunctionApp
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class Function1
    {
        [FunctionName("Function1")]
        public static async Task Run([QueueTrigger("myqueue-items", Connection = "AzureWebJobsStorage")] string message,
                                     [Inject] IStorageDataProvider storage)
        {
            await storage.AddDataToBlob("blob1", "Blob" + message, message);
        }
    }
}