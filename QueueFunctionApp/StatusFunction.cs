using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AzureFunctions.Autofac;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace QueueFunctionApp
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class StatusFunction
    {
        [FunctionName("StatusFunction")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "status")] HttpRequest req,
            [Inject] IStorageDataProvider storage)
        {
            

            string status = req.Query["status"];
            if (status == "clear")
            {
                await storage.ClearQueue("items");
            }
            else if (status == "start")
            {
                var messages = await storage.GetAllMessagesFromQueue("items", 200);
                var requestBodies =
                    messages.Select(x => JsonConvert.DeserializeObject<HttpTrigger.RequestBody>(x.AsString)).ToList();
                await storage.AddDataToBlob("items1", "console",
                    JsonConvert.SerializeObject(requestBodies.Where(x =>
                        x.RequestType == HttpTrigger.RequestType.Console)));
                await storage.AddDataToBlob("items1", "http",
                    JsonConvert.SerializeObject(requestBodies.Where(x =>
                        x.RequestType == HttpTrigger.RequestType.Http)));

            }
           
            return status != null
                ? (ActionResult)new OkObjectResult($"Hello, {status}")
                : new BadRequestObjectResult("Please pass a name on the query string or in the request body");
        }
    }
}
