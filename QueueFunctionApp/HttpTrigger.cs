using System;
using System.IO;
using System.Threading.Tasks;
using AzureFunctions.Autofac;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace QueueFunctionApp
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class HttpTrigger
    {
        [FunctionName("HttpTrigger")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function,  "post", Route = "save")] HttpRequest req,
            [Inject]IStorageDataProvider storage)
        {



            string name = req.Query["name"];
           var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var data = JsonConvert.DeserializeObject<RequestBody>(requestBody);
            await storage.AddMessageToQueue("items", new CloudQueueMessage(requestBody));
            return name != null
                ? (ActionResult)new OkObjectResult($"Hello, {name}")
                : new BadRequestObjectResult("Please pass a name on the query string or in the request body");
        }
        public class RequestBody
        {
            public RequestType RequestType { get; set; } = RequestType.Http;
            public string Message { get; set; }
        }
        public enum RequestType
        {
            Console,
            Http
        }
    }
}
