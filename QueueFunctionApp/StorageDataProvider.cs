﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;

namespace QueueFunctionApp
{
    public class StorageDataProvider : IStorageDataProvider
    {
        private CloudBlobClient BlobClient => _storageAccount.CreateCloudBlobClient();

        private CloudTableClient TableClient => _storageAccount.CreateCloudTableClient();

        private CloudQueueClient QueueClient => _storageAccount.CreateCloudQueueClient();

        private readonly CloudStorageAccount _storageAccount;

        public StorageDataProvider()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();


            _storageAccount = CloudStorageAccount.DevelopmentStorageAccount;
        }

        public async Task<IEnumerable<CloudQueueMessage>> GetAllMessagesFromQueue(string queueName, int messageCount)
        {
            var queue = QueueClient.GetQueueReference(queueName);
            return await queue.GetMessagesAsync(messageCount);
        }

        public async Task AddMessagesToQueue(string queueName, IEnumerable<CloudQueueMessage> messages)
        {
            var queue = QueueClient.GetQueueReference(queueName);
            await queue.CreateIfNotExistsAsync();
            foreach (var cloudQueueMessage in messages)
            {
                await queue.AddMessageAsync(cloudQueueMessage);
            }
        }

        public async Task AddMessageToQueue(string queueName, CloudQueueMessage message)
        {
            var queue = QueueClient.GetQueueReference(queueName);
            await queue.CreateIfNotExistsAsync();
            await queue.AddMessageAsync(message);
        }

        public async Task ClearQueue(string queueName)
        {
            var queue = QueueClient.GetQueueReference(queueName);
           await queue.ClearAsync();
        }

        public async Task AddDataToBlob(string blobContainer, string blobName, string content)
        {
            var container = BlobClient.GetContainerReference(blobContainer);
            await container.CreateIfNotExistsAsync();
            var blob = container.GetBlockBlobReference(blobName);
            await blob.UploadTextAsync(content);

        }

        public async Task<string> GetAllContentFromBlob(string blobContainer, string blobName)
        {
            var container = BlobClient.GetContainerReference(blobContainer);
            var blob = container.GetBlockBlobReference(blobName);
            return await blob.DownloadTextAsync();
        }

        public async Task<List<string>> GetAllContentFromBlob(string blobContainer)
        {
            var container = BlobClient.GetContainerReference(blobContainer);
            var blob = await container.ListBlobsSegmentedAsync(new BlobContinuationToken());
            var items = new List<string>();

            foreach (var item in blob.Results)
            {
                var cloudBlob = await BlobClient.GetBlobReferenceFromServerAsync(item.Uri);
                var blockBlob = container.GetBlockBlobReference(cloudBlob.Name);
                items.Add(await blockBlob.DownloadTextAsync());
                //  var stream = new MemoryStream();
                // await cloudBlob.(stream);
                // items.Add( new StreamReader(stream).ReadToEnd());
            }

            return items;
        }
    }
}