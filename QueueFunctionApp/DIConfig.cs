﻿using Autofac;
using AzureFunctions.Autofac.Configuration;

namespace QueueFunctionApp
{
    public class DIConfig
    {
        public DIConfig(string functionName)
        {
            DependencyInjection.Initialize(builder =>
            {
                builder.RegisterType<StorageDataProvider>().As<IStorageDataProvider>();
            }, functionName);
        }
    }
}