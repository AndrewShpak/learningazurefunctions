﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter some text:");
            var value = Console.ReadLine();
            var client = new RestSharp.RestClient("http://localhost:7071/api");
            var request = new RestRequest("/save", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new RequestBody
            {
                Message = value,
            }); 
            client.ExecuteAsync(request, response => {
                Console.WriteLine(response.Content);
            });
            Console.ReadKey();
        }

        public class RequestBody
        {
            public RequestType RequestType { get; set; } = RequestType.Console;
            public string Message { get; set; }
        }
       public enum RequestType
        {
            Console,
            Http
        }
    }
}
